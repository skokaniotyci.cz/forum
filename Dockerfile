FROM php:7.3-apache

ENV VANILLA_VERSION 3.3
ENV VANILLA_FOLDER /forum

EXPOSE 8000

WORKDIR /tmp/build-deps

RUN useradd vanilla \
    && apt-get update && apt-get install -y unzip libpng-dev nano libz-dev libmemcached-dev libicu-dev \
    && pecl install memcached \
    && echo extension=memcached.so >> /usr/local/etc/php/conf.d/memcached.ini \
    && docker-php-ext-configure intl \
    && docker-php-ext-install pdo pdo_mysql gd intl \
    && curl -sSL "https://github.com/vanilla/vanilla/releases/download/Vanilla_${VANILLA_VERSION}/vanilla-${VANILLA_VERSION}.zip" -o vanilla.zip \
    && unzip vanilla.zip \
    && cp -rT package /var/www/html${VANILLA_FOLDER} \
    && chmod -R 777 /var/www/html${VANILLA_FOLDER}/conf \
    && chmod -R 777 /var/www/html${VANILLA_FOLDER}/cache \
    && mv /var/www/html${VANILLA_FOLDER}/.htaccess.dist /var/www/html${VANILLA_FOLDER}/.htaccess \
    && a2enmod rewrite \
    && a2enmod headers \
    && curl -sSL "https://us.v-cdn.net/5018160/uploads/addons/9DYTNYNZBAJA.zip" -o whosonline.zip \
    && curl -sSL "https://us.v-cdn.net/5018160/uploads/addons/v5cqtnwiw8tu.zip" -o latest-comment.zip \
    && unzip whosonline.zip \
    && unzip latest-comment.zip \
    && sed -i 's/vanilla2/forum/g' LatestComment/class.latestcommentmodule.php \
    && sed -i 's/LatestComment.LatestOrMost/LatestComment.Show.LatestComment/g' LatestComment/class.latestcommentmodule.php \
    && mv WhosOnline /var/www/html${VANILLA_FOLDER}/plugins/ \
    && mv LatestComment /var/www/html${VANILLA_FOLDER}/plugins/ \
    && apt-get purge -y unzip libpng-dev libz-dev libmemcached-dev libicu-dev \
    && rm -rf /tmp/build-deps

WORKDIR /var/www/html

COPY themes/ /var/www/html${VANILLA_FOLDER}/themes/
COPY locales/ /var/www/html${VANILLA_FOLDER}/locales/
COPY apache2/sites-available/000-default.conf /etc/apache2/sites-available/
COPY apache2/ports.conf /etc/apache2/

USER vanilla

CMD ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]
