<!DOCTYPE html>
<html lang="{$CurrentLocale.Lang}">
<head>
  <meta name="robots" content="noindex">
  {asset name="Head"}
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body id="{$BodyID}" class="{$BodyClass}">

    <div id="Frame">
        <div class="Head" id="Head" role="banner">
            <div class="Row">
                <strong class="SiteTitle"><a href="{link path="/"}">{logo}</a></strong>
                <div class="SiteSearch" role="search">{searchbox}</div>
                <ul class="SiteMenu">
                    <li><a href="/">Hlavní strana</a></li>
                    {discussions_link}
                    {activity_link}
                    {custom_menu}
                </ul>
            </div>
        </div>
        <div id="Body">
            <div class="Row">
                <div class="BreadcrumbsWrapper">{breadcrumbs}</div>
                <div class="Column PanelColumn" id="Panel" role="complementary">
                    {module name="MeModule"}
                    {asset name="Panel"}
                </div>
                <div class="Column ContentColumn" id="Content" role="main">{asset name="Content"}</div>
            </div>
        </div>
        <div id="Foot" role="contentinfo">
            <div class="Row">
                <a href="{vanillaurl}" class="PoweredByVanilla" title="Community Software by Vanilla Forums">Forum Software
                    Powered by Vanilla</a>
                {asset name="Foot"}
            </div>
        </div>
   </div>

   <div style="display:none;" class="nav_up" id="nav_up" title="nahoru">
     <i class="fa fa-chevron-up"></i>
   </div>
   <div style="display:none;" class="nav_comments" id="nav_comments" title="komentáře">
     <i class="fa fa-comments"></i>
   </div>
   <div style="display:none;" class="nav_down" id="nav_down" title="dolů">
     <i class="fa fa-chevron-down"></i>
   </div>

   {event name="AfterBody"}
</body>
</html>
